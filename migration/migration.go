package migration

import (
	"errors"
	"ewallet/util"
	"fmt"
	"io/ioutil"
	"log"

	"ewallet/appcontext"

	"github.com/jmoiron/sqlx"
)

var Db *sqlx.DB
var basepath = util.GetRootFolderPath()

func RunMigration(updown string) error {
	Db = appcontext.GetDB()

	migrationQueryString, err := GetStringFromFile(basepath + "migration/" + updown + ".sql")
	fmt.Println(migrationQueryString)
	if err != nil {
		return err
	}

	_, err = Db.Exec(*migrationQueryString)

	if err != nil {
		return err
	}
	fmt.Println("Migration successful")
	return nil
}

func RunSeeder() error {
	Db = appcontext.GetDB()

	seederQueryString, err := GetStringFromFile(basepath + "migration/seeder.sql")
	fmt.Println(seederQueryString)
	if err != nil {
		return err
	}

	_, err = Db.Exec(*seederQueryString)

	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err)
	}
	return nil
}

func GetStringFromFile(filename string) (*string, error) {
	content, err := ioutil.ReadFile(filename)

	if err != nil {
		return nil, errors.New("File not found")
	}

	queryString := fmt.Sprintf("%s", content)

	return &queryString, nil
}
