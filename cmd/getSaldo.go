package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

var getSaldoCmd = &cobra.Command{
	Use:   "get_saldo",
	Short: "get_saldo from a branch",
	Long:  `get_saldo from a branch`,
	Run: func(cmd *cobra.Command, args []string) {
		respCabang, err := http.Post("http://172.22.0.217/ewallet/getSaldo", "application/json", bytes.NewBuffer([]byte(`{"user_id":"1506726946"}`)))
		if err == nil {
			defer respCabang.Body.Close()
			contents, _ := ioutil.ReadAll(respCabang.Body)

			var contentsMap map[string]interface{}

			_ = json.Unmarshal(contents, &contentsMap)
			if int(contentsMap["saldo"].(float64)) == -2 {
				fmt.Println("quorum tidak lengkap")
				return
			} else if int(contentsMap["saldo"].(float64)) >= 0 {
				fmt.Printf("Saldo anda di cabang ini: %d\n", int(contentsMap["saldo"].(float64)))
			}
		} else {
			fmt.Println(err.Error())
			return
		}
	},
}

func init() {
	rootCmd.AddCommand(getSaldoCmd)

	getSaldoCmd.Flags().StringVarP(&userId, "user_id", "u", "", "id for your user")
}
