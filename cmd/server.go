package cmd

import (
	"ewallet/server"
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "run ewallet server",
	Long:  `run ewallet server`,
	Run: func(cmd *cobra.Command, args []string) {
		router := server.CreateRouter()
		fmt.Println("LISTENING ON PORT :80")
		log.Fatal(http.ListenAndServe(":80", router))
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
