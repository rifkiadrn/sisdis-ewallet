package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

var updateQuorumCmd = &cobra.Command{
	Use:   "update-quorum",
	Short: "update-quorum list",
	Long:  `update-quorum list`,
	Run: func(cmd *cobra.Command, args []string) {
		respCabang, err := http.Get("http://172.22.0.215/ewallet/list")
		if err == nil {
			defer respCabang.Body.Close()
			contents, _ := ioutil.ReadAll(respCabang.Body)

			ioutil.WriteFile("quorum.json", contents, 0)
		} else {
			fmt.Println(err.Error())
			return
		}
	},
}

func init() {
	rootCmd.AddCommand(updateQuorumCmd)
}
