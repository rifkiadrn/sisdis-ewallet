package cmd

import (
	"bufio"
	"bytes"
	"encoding/json"
	"ewallet/appcontext"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/spf13/cobra"
)

var nilai int

var transferCmd = &cobra.Command{
	Use:   "transfer",
	Short: "transfer ewallet money from your branch to another branch",
	Long:  `transfer ewallet money from your branch to another branch`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("transfer called")
		data, err := appcontext.GetHelper().DoQueryRow("SELECT saldo FROM users WHERE user_id=$1", "1506726946")
		if err == nil {
			if int(data["saldo"].(int64)) > nilai && nilai >= 0 {
				ip := ""
				for _, element := range appcontext.GetQuorum() {
					if element["npm"] == userId {
						ip = element["ip"]
						break
					}
				}
				respCabang, err := http.Post("http://"+ip+"/ewallet/getSaldo", "application/json", bytes.NewBuffer([]byte(`{"user_id":"1506726946"}`)))
				if err == nil {
					defer respCabang.Body.Close()
					contents, _ := ioutil.ReadAll(respCabang.Body)
					var contentsMap map[string]interface{}
					_ = json.Unmarshal(contents, &contentsMap)
					if int(contentsMap["saldo"].(float64)) == -1 {
						var userIdInput string
						var nameInput string
						for {
							fmt.Print("User id: ")
							reader := bufio.NewReader(os.Stdin)
							userIdInput, _ = reader.ReadString('\n')
							userIdInput = string(userIdInput[0 : len(userIdInput)-1])
							if userIdInput != "" {
								break
							}
						}
						for {
							fmt.Print("nama: ")
							reader := bufio.NewReader(os.Stdin)
							nameInput, _ = reader.ReadString('\n')
							nameInput = string(nameInput[0 : len(nameInput)-1])
							if nameInput != "" {
								break
							}
						}
						respRegisterCabang, err := http.Post("http://"+ip+"/ewallet/register", "application/json", bytes.NewBuffer([]byte(`{"user_id":"`+userIdInput+`", "nama": "`+nameInput+`"}`)))
						if err == nil {
							defer respRegisterCabang.Body.Close()
							contents, _ := ioutil.ReadAll(respRegisterCabang.Body)
							_ = json.Unmarshal(contents, &contentsMap)
							if int(contentsMap["registerReturn"].(float64)) != 1 {
								fmt.Println("server return for register is not 1")
								return
							} else if int(contentsMap["registerReturn"].(float64)) == 1 {
								fmt.Println("register berhasil")
							}
						} else {
							fmt.Println(err.Error())
							return
						}

					}
				} else {
					fmt.Println(err.Error())
					return
				}
				respTransferCabang, err := http.Post("http://"+ip+"/ewallet/transfer", "application/json", bytes.NewBuffer([]byte(`{"user_id":"1506726946","nilai":`+fmt.Sprint(nilai)+`}`)))
				if err == nil {
					defer respTransferCabang.Body.Close()
					contents, _ := ioutil.ReadAll(respTransferCabang.Body)
					var res map[string]interface{}
					_ = json.Unmarshal(contents, &res)
					fmt.Println(res)
					if int(res["transferReturn"].(float64)) == 1 {

						fmt.Printf("Transfer sebesar %d berhasil\n", nilai)
						data, err := appcontext.GetHelper().DoQueryRow("UPDATE users set saldo = saldo - $2 WHERE user_id=$1 RETURNING *", "1506726946", nilai)

						if err != nil {
							fmt.Println(err.Error())
							return
						}
						if data != nil {
							fmt.Printf("Saldo anda dicabang ini telah dikurangi sebesar %d\n", nilai)
							fmt.Printf("Saldo anda saat ini %d\n", data["saldo"])
						}
						return
					}

				} else {
					fmt.Println(err.Error())
					return
				}
			} else {
				fmt.Println("Saldo tidak cukup atau nilai transfer harus diantara 0 - 1M")
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(transferCmd)

	transferCmd.Flags().StringVarP(&userId, "user_id", "u", "", "id for your user")
	transferCmd.Flags().IntVarP(&nilai, "nilai", "n", 0, "saldo to be transfered")
}
