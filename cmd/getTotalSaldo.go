package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

var getTotalSaldoCmd = &cobra.Command{
	Use:   "get_total_saldo",
	Short: "get_total_saldo on your ewallet system",
	Long:  `get_total_saldo on your ewallet system`,
	Run: func(cmd *cobra.Command, args []string) {
		respCabang, err := http.Post("http://172.22.0.217/ewallet/getTotalSaldo", "application/json", bytes.NewBuffer([]byte(`{"user_id":"1506726946"}`)))
		if err == nil {
			defer respCabang.Body.Close()
			contents, _ := ioutil.ReadAll(respCabang.Body)

			var contentsMap map[string]interface{}

			_ = json.Unmarshal(contents, &contentsMap)
			if int(contentsMap["saldo"].(float64)) == -2 {
				fmt.Println("quorum tidak lengkap")
				return
			} else if int(contentsMap["saldo"].(float64)) == -3 {
				fmt.Println("salah satu cabang ada yang error")
				return
			} else if int(contentsMap["saldo"].(float64)) >= 0 {
				fmt.Printf("Saldo total anda diseluruh cabang adalah: %d\n", int(contentsMap["saldo"].(float64)))
			}
		} else {
			fmt.Println(err.Error())
			return
		}
	},
}

func init() {
	rootCmd.AddCommand(getTotalSaldoCmd)

	getTotalSaldoCmd.Flags().StringVarP(&userId, "user_id", "u", "", "id for your user")
}
