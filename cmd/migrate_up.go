package cmd

import (
	"ewallet/migration"
	"fmt"

	"github.com/spf13/cobra"
)

var migrateUpCmd = &cobra.Command{
	Use:   "migrate-up",
	Short: "migrate-up the database",
	Long:  `migrate-up the database`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("migrating...")
		migration.RunMigration("up")
	},
}

func init() {
	rootCmd.AddCommand(migrateUpCmd)
}
