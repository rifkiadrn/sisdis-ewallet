package cmd

import (
	"ewallet/migration"

	"github.com/spf13/cobra"
)

var migrateDownCmd = &cobra.Command{
	Use:   "migrate-down",
	Short: "migrate-down the database",
	Long:  `migrate-down the database`,
	Run: func(cmd *cobra.Command, args []string) {
		migration.RunMigration("down")
	},
}

func init() {
	rootCmd.AddCommand(migrateDownCmd)
}
