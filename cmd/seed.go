package cmd

import (
	"ewallet/migration"

	"github.com/spf13/cobra"
)

var seedCmd = &cobra.Command{
	Use:   "seed",
	Short: "seed the database",
	Long:  `seed the database`,
	Run: func(cmd *cobra.Command, args []string) {
		migration.RunSeeder()
	},
}

func init() {
	rootCmd.AddCommand(seedCmd)
}
