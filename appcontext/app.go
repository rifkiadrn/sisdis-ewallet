package appcontext

import (
	"encoding/json"
	"ewallet/config"
	"ewallet/helper"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var context *appContext

type appContext struct {
	db     *sqlx.DB
	helper helper.QueryExecuter
	quorum []map[string]string
}

func InitContext() {
	db, err := initDB()
	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err.Error())
	}
	quorumList, err := initQuorum()

	context = &appContext{
		db: db,
		helper: &helper.QueryHelper{
			DB: db,
		},
		quorum: quorumList,
	}
}

func initQuorum() ([]map[string]string, error) {
	file, err := ioutil.ReadFile("./quorum.json")
	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err)
	}
	var contentList []map[string]string
	_ = json.Unmarshal(file, &contentList)
	return contentList, nil
}

func initDB() (*sqlx.DB, error) {

	db, err := sqlx.Open("postgres", config.GetConnectionString())
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}

func GetDB() *sqlx.DB {
	return context.db
}
func GetHelper() helper.QueryExecuter {
	return context.helper
}
func GetQuorum() []map[string]string {
	return context.quorum
}
