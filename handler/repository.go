package handler

import (
	"ewallet/helper"
)

const (
	registerQuery = "INSERT INTO users(user_id,nama,saldo) values($1,$2,0) RETURNING *"
	getSaldoQuery = "SELECT saldo FROM users WHERE user_id = $1"
	transferQuery = "UPDATE users set saldo = saldo + $2 where user_id = $1 RETURNING *"
)

type Repository interface {
	registerRepo(userId, nama string) int
	transferRepo(userId string, saldo int) int
	getSaldoRepo(userId string) int
}

type repository struct {
	execer helper.QueryExecuter
}

func initRepository(execer helper.QueryExecuter) Repository {
	return &repository{
		execer: execer,
	}
}

func (r *repository) registerRepo(userId, nama string) int {
	_, err := r.execer.DoQueryRow(registerQuery, userId, nama)
	if err != nil {
		return -4
	}
	return 1
}

func (r *repository) transferRepo(userId string, saldo int) int {
	data, err := r.execer.DoQueryRow(transferQuery, userId, saldo)
	if err != nil {
		return -4
	} else if data == nil {
		return -1
	}
	return 1
}

func (r *repository) getSaldoRepo(userId string) int {
	data, err := r.execer.DoQueryRow(getSaldoQuery, userId)
	if err != nil {
		return -4
	} else if data["saldo"] == nil {
		return -1
	}
	return int(data["saldo"].(int64))
}
