package handler

import (
	"encoding/json"
	response "ewallet/helper"
	"fmt"
	"net/http"
)

var proc Processor

func getProcessor() Processor {
	if proc == nil {
		return NewProcessor()
	}
	return proc
}

func PingHandler(w http.ResponseWriter, r *http.Request) {
	resp := make(map[string]interface{})
	status := getProcessor().ping()
	resp["pingReturn"] = status
	respJson, _ := json.Marshal(resp)

	response.ResponseHelper(w, http.StatusOK, response.ContentJson, string(respJson))
}

func GetSaldoHandler(w http.ResponseWriter, r *http.Request) {
	var payload map[string]string
	err := json.NewDecoder(r.Body).Decode(&payload)
	var status int
	resp := make(map[string]interface{})
	if err != nil {
		status = -99
	} else {
		status = getProcessor().getSaldo(payload["user_id"])
	}
	resp["saldo"] = status
	respJson, _ := json.Marshal(resp)
	response.ResponseHelper(w, http.StatusOK, response.ContentJson, string(respJson))
}

func GetTotalSaldoHandler(w http.ResponseWriter, r *http.Request) {
	var payload map[string]string
	err := json.NewDecoder(r.Body).Decode(&payload)
	var status int
	resp := make(map[string]interface{})

	if err != nil {
		status = -99
	} else {
		status = getProcessor().getTotalSaldo(payload["user_id"])
	}
	resp["saldo"] = status
	respJson, _ := json.Marshal(resp)
	response.ResponseHelper(w, http.StatusOK, response.ContentJson, string(respJson))
}

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	var payload map[string]string
	err := json.NewDecoder(r.Body).Decode(&payload)
	var status int
	if err != nil {
		status = -99
	} else {
		status = getProcessor().register(payload["user_id"], payload["nama"])
	}

	resp := make(map[string]interface{})
	resp["RegisterReturn"] = status

	respJson, _ := json.Marshal(resp)
	response.ResponseHelper(w, http.StatusOK, response.ContentJson, string(respJson))
}

func TransferHandler(w http.ResponseWriter, r *http.Request) {
	var payload map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&payload)
	var status int
	if err != nil {
		status = -99
	} else {
		status = getProcessor().transfer(fmt.Sprint(payload["user_id"]), int(payload["nilai"].(float64)))
	}
	resp := make(map[string]interface{})
	resp["transferReturn"] = status

	respJson, _ := json.Marshal(resp)
	response.ResponseHelper(w, http.StatusOK, response.ContentJson, string(respJson))
}
