package handler

import (
	"bytes"
	"encoding/json"
	"ewallet/appcontext"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Processor interface {
	ping() int
	register(userId, nama string) int
	transfer(userId string, saldo int) int
	getSaldo(userId string) int
	getTotalSaldo(userId string) int
}

type processor struct {
	repo Repository
}

func NewProcessor() Processor {
	return &processor{
		repo: initRepository(appcontext.GetHelper()),
	}
}

func (p *processor) ping() int {
	return 1
}

func (p *processor) register(userId, nama string) int {
	status := p.repo.registerRepo(userId, nama)
	return status
}

func (p *processor) transfer(userId string, saldo int) int {
	if saldo < 0 || saldo > 1000000000 {
		return -5
	}
	status := p.repo.transferRepo(userId, saldo)
	return status
}
func (p *processor) getSaldo(userId string) int {
	status := p.repo.getSaldoRepo(userId)
	return status
}

func (p *processor) getTotalSaldo(userId string) int {
	file, err := ioutil.ReadFile("./quorum.json")
	if err != nil {
		fmt.Println(err.Error())
		return -99
	}
	var contentList []map[string]string
	_ = json.Unmarshal(file, &contentList)

	total := p.repo.getSaldoRepo(userId)
	if total == -1 {
		return -1
	}
	body := []byte(`{"user_id": "` + userId + `"}`)
	for _, element := range contentList {
		if element["npm"] != "1506726946" {
			respCabang, err := http.Post("http://"+element["ip"]+"/ewallet/getSaldo", "application/json", bytes.NewBuffer(body))
			if err != nil {
				return -3
			}
			defer respCabang.Body.Close()
			contents, _ := ioutil.ReadAll(respCabang.Body)
			var contentsMap map[string]interface{}
			_ = json.Unmarshal(contents, &contentsMap)
			if contentsMap["saldo"] == nil {
				return -3
			}
			total = total + int(contentsMap["saldo"].(float64))
		}
	}
	return total
}
