package server

import (
	"ewallet/handler"

	"github.com/gorilla/mux"
)

func CreateRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/ewallet/ping", handler.PingHandler).Methods("POST")
	router.HandleFunc("/ewallet/register", CheckQuorumVal(handler.RegisterHandler, "register")).Methods("POST")
	router.HandleFunc("/ewallet/getSaldo", CheckQuorumVal(handler.GetSaldoHandler, "getSaldo")).Methods("POST")
	router.HandleFunc("/ewallet/getTotalSaldo", CheckQuorumVal(handler.GetTotalSaldoHandler, "getTotalSaldo")).Methods("POST")
	router.HandleFunc("/ewallet/transfer", CheckQuorumVal(handler.TransferHandler, "transfer")).Methods("POST")

	return router
}
