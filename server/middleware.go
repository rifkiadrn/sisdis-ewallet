package server

import (
	"bytes"
	"encoding/json"
	"ewallet/appcontext"
	"ewallet/helper"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"
	"sync"
)

func CheckQuorumVal(next http.HandlerFunc, handlerName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		quorumVal := 0
		quorum := appcontext.GetQuorum()
		minimumQuorum := int(math.Ceil(float64(len(quorum))/2.0)) + 1

		var wg sync.WaitGroup
		wg.Add(len(quorum))

		for _, element := range quorum {
			go func(element map[string]string) {
				defer wg.Done()
				respCabang, err := http.Post("http://"+element["ip"]+"/ewallet/ping", "application/json", bytes.NewBuffer([]byte(`{}`)))
				if err == nil {
					defer respCabang.Body.Close()
					contents, _ := ioutil.ReadAll(respCabang.Body)
					var contentsMap map[string]interface{}
					_ = json.Unmarshal(contents, &contentsMap)

					if contentsMap["pingReturn"] != nil {
						fmt.Println(contentsMap["pingReturn"])
						i, _ := strconv.Atoi(fmt.Sprint(contentsMap["pingReturn"]))
						quorumVal = quorumVal + i
					}
					fmt.Println(contentsMap)
				}
			}(element)
		}
		wg.Wait()

		fmt.Println("QUORUM : " + fmt.Sprint(quorumVal))

		if quorumVal == len(quorum) {
			next.ServeHTTP(w, r)
		} else if quorumVal >= minimumQuorum {
			if handlerName != "getTotalSaldo" {
				next.ServeHTTP(w, r)
			} else {
				fmt.Println("Unauthorized")
				errorMessage, _ := json.Marshal(map[string]int{"saldo": -2})
				helper.ResponseHelper(w, 401, helper.ContentJson, string(errorMessage))
			}
		} else {
			fmt.Println("Unauthorized")
			errorMessage, _ := json.Marshal(map[string]int{"saldo": -2})
			helper.ResponseHelper(w, 401, helper.ContentJson, string(errorMessage))
		}
	}
}
