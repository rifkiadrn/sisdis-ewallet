package main

import (
	"ewallet/appcontext"
	"ewallet/cmd"
)

func main() {
	appcontext.InitContext()
	cmd.Execute()
}
